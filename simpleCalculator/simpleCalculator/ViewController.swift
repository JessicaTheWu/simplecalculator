//
//  ViewController.swift
//  simpleCalculator
//
//  Created by Jessica on 2018/2/22.
//  Copyright © 2018年 Jessica. All rights reserved.
//

import UIKit
enum Operation:String {
    case Plus = "+"
    case Minus = "-"
    case Divide = "/"
    case Multiply = "*"
    case NULL = "Null"
}
class ViewController: UIViewController {
    var dotCount:Int = 0
    
    var showNumber = ""
    var leftNumber = ""
    var rightNumber = ""
    var total = ""
    var currentOperation:Operation = .NULL
    
    @IBOutlet weak var result: UILabel!

    @IBAction func resetButton(_ sender: UIButton) {
        dotCount = 0
        result.text = "0";
        showNumber = "0"
        leftNumber = ""
        rightNumber = ""
        total = ""
        currentOperation = .NULL
    }
    
    @IBAction func numberButton(_ sender: UIButton) {
        
        if result.text == "0" && sender.tag == 0{
           result.text = "0"
        }else if showNumber == "0"{
            showNumber = "\(Int(showNumber)! * 10 + sender.tag)"
            result.text = showNumber
        }else if showNumber.count <= 14{
            print(sender.tag)
            showNumber += "\(sender.tag)"
            result.text = showNumber
        }
    }
    @IBAction func dotButton(_ sender: UIButton) {
        
        
        print(dotCount)
        if result.text == "0"{
            print("haha")
            //showNumber += "0"
            //result.text = showNumber
            
        }
        if dotCount == 0 {
            showNumber += "."
            result.text = showNumber
            dotCount = dotCount + 1
            print(dotCount)
            
        }
    }
    
    @IBAction func equalButton(_ sender: UIButton) {
        operation(operation: currentOperation)
    }
    @IBAction func plusButton(_ sender: UIButton) {
        operation(operation: .Plus)
        

    }
    @IBAction func minusButton(_ sender: UIButton) {
        operation(operation: .Minus)

    }
    @IBAction func divideButton(_ sender: UIButton) {
        operation(operation: .Divide)

    }
    
    @IBAction func multipButton(_ sender: UIButton) {
        operation(operation: .Multiply)
       

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        result.text = "0"
        showNumber = "0"
        
    }
    func operation(operation: Operation){
        if currentOperation != .NULL {
            if showNumber != "" {
                rightNumber = showNumber
                showNumber = ""
                
                if currentOperation == .Plus {
                    total = "\(Double(leftNumber)! + Double(rightNumber)!)"
                }else if currentOperation == .Minus {
                    total = "\(Double(leftNumber)! - Double(rightNumber)!)"
                }else if currentOperation == .Multiply {
                    total = "\(Double(leftNumber)! * Double(rightNumber)!)"
                }else if currentOperation == .Divide {
                    total = "\(Double(leftNumber)! / Double(rightNumber)!)"
                }
                leftNumber = total
                if (Double(total)!.truncatingRemainder(dividingBy: 1) == 0) {
                    total = "\(Int(Double(total)!))"
                }
                result.text = total
            }
            currentOperation = operation
           
        }else {
            leftNumber = showNumber
            showNumber = ""
            currentOperation = operation
        }
        dotCount = 0
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

